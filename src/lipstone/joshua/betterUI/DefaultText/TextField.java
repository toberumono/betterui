package lipstone.joshua.betterUI.DefaultText;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class TextField extends JTextField {
	private String defaultText;
	private Color defaultTextColor, userTextColor;
	private final FocusListener fl;
	private final KeyListener kl;
	private final MouseListener ml;
	private boolean keyPressed = false;
	private static final String[] keystrokeNames = {"UP", "DOWN", "LEFT", "RIGHT"};
	
	/**
	 * Creates a new TextField with the given default text, and {@link java.awt.Color#gray gray} as the color it applies to
	 * the default text, and {@link java.awt.Color#black black} as the color it applies to the user's text.
	 * 
	 * @param text
	 *            the default text
	 */
	public TextField(String text) {
		this(text, Color.gray, Color.black);
	}
	
	/**
	 * Creates a new TextField with the given default text, default text {@link java.awt.Color Color}, and user text
	 * {@link java.awt.Color Color}
	 * 
	 * @param text
	 *            the default text
	 * @param defaultTextColor
	 *            the default text color
	 * @param userTextColor
	 *            the user text color
	 */
	public TextField(String text, final Color defaultTextColor, final Color userTextColor) {
		super(text);
		this.defaultTextColor = defaultTextColor;
		this.userTextColor = userTextColor;
		defaultText = text;
		setForeground(defaultTextColor);
		for (int i = 0; i < keystrokeNames.length; i++)
			getInputMap().put(KeyStroke.getKeyStroke(keystrokeNames[i]), "none");
		wipeCaret();
		addFocusListener((fl = new FocusListener() {
			
			@Override
			public void focusGained(FocusEvent e) {
				wipeCaret();
			}
			
			@Override
			public void focusLost(FocusEvent e) {}
			
		}));
		addKeyListener((kl = new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				if (keyPressed) {
					setText("");
					setForeground(userTextColor);
					getCaret().setSelectionVisible(true);
					for (int i = 0; i < keystrokeNames.length; i++)
						getInputMap().remove(KeyStroke.getKeyStroke(keystrokeNames[i]));
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (getForeground().equals(defaultTextColor) && getText(true).equals(defaultText) && e.getKeyCode() != 8 && e.getKeyCode() != 37 && e.getKeyCode() != 38 && e.getKeyCode() != 39 && e.getKeyCode() != 40)
					keyPressed = true;
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				keyPressed = false;
				if (getText(true).equals("")) {
					setForeground(defaultTextColor);
					setText(defaultText);
					for (int i = 0; i < keystrokeNames.length; i++)
						getInputMap().put(KeyStroke.getKeyStroke(keystrokeNames[i]), "none");
					setCaretPosition(0);
					getCaret().setSelectionVisible(false);
				}
			}
		}));
		addMouseListener((ml = new MouseListener() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				wipeCaret();
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				wipeCaret();
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				wipeCaret();
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
		}));
	}
	
	@Override
	public void removeFocusListener(FocusListener l) {
		if (!l.equals(fl))
			super.removeFocusListener(l);
	}
	
	@Override
	public void removeKeyListener(KeyListener l) {
		if (!l.equals(kl))
			super.removeKeyListener(l);
	}
	
	@Override
	public void removeMouseListener(MouseListener l) {
		if (!l.equals(ml))
			super.removeMouseListener(l);
	}
	
	/**
	 * Resets this text field to the default text
	 */
	public void clear() {
		setForeground(defaultTextColor);
		setText(defaultText);
		setCaretPosition(0);
		getCaret().setSelectionVisible(false);
	}
	
	private void wipeCaret() {
		if (super.getText().equals(defaultText) && getForeground().equals(defaultTextColor)) {
			setCaretPosition(0);
			getCaret().setSelectionVisible(false);
		}
	}
	
	/**
	 * @return the default text for this field
	 */
	public String getDefaultText() {
		return defaultText;
	}
	
	/**
	 * Change the default text for this field
	 * 
	 * @param defaultText
	 *            the new text to use as the default text
	 */
	public void setDefaultText(String defaultText) {
		if (getForeground().equals(defaultTextColor))
			setText(defaultText);
		this.defaultText = defaultText;
	}
	
	/**
	 * @return the {@link java.awt.Color Color} to use when the user has not yet typed in this field
	 */
	public Color getDefaultTextColor() {
		return defaultTextColor;
	}
	
	/**
	 * @param defaultTextColor
	 *            the {@link java.awt.Color Color} to use when the user has not yet typed in this field
	 */
	public void setDefaultTextColor(Color defaultTextColor) {
		if (getForeground().equals(this.defaultTextColor))
			setForeground(defaultTextColor);
		this.defaultTextColor = defaultTextColor;
	}
	
	/**
	 * @return the {@link java.awt.Color Color} to use when the user has typed in this field
	 */
	public Color getUserTextColor() {
		return userTextColor;
	}
	
	/**
	 * @param userTextColor
	 *            the {@link java.awt.Color Color} to use when the user has typed in this field
	 */
	public void setUserTextColor(Color userTextColor) {
		if (getForeground().equals(this.userTextColor))
			setForeground(userTextColor);
		this.userTextColor = userTextColor;
	}
	
	private final String getText(boolean includeDefault) {
		return includeDefault ? super.getText() : getText();
	}
	
	/**
	 * @return an empty string if this <tt>TextField</tt> contains the default text, otherwise the text within this
	 *         <tt>TextField</tt>
	 */
	@Override
	public String getText() {
		return getForeground().equals(userTextColor) ? super.getText() : "";
	}
	
	/**
	 * @return whether the default text is currently the contents of this <tt>TextField</tt>
	 */
	public boolean isDefaultText() {
		return getForeground().equals(defaultTextColor) && getText(true).equals(defaultText);
	}
}
