package lipstone.joshua.betterUI.suggestion;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;

public abstract class SuggestingTextArea extends JLayeredPane {
	private static final KeyStroke[] keystrokes = {KeyStroke.getKeyStroke("UP"), KeyStroke.getKeyStroke("DOWN"), KeyStroke.getKeyStroke("LEFT"), KeyStroke.getKeyStroke("RIGHT"), KeyStroke.getKeyStroke("alt RIGHT"),
			KeyStroke.getKeyStroke("alt LEFT")};
	private JTextArea textArea = new JTextArea();
	private boolean suggesting = false;
	private int suggestionIndex = 0, selectedSuggestion = 0;
	public int fontWidth = textArea.getFontMetrics(textArea.getFont()).getHeight() - 2, fontHeight = textArea.getFontMetrics(textArea.getFont()).getHeight();
	protected int suggestionRoot = 0, suggestionMax = 9;
	protected DefaultListModel<String> suggestions = new DefaultListModel<String>();
	private SuggestionPanel suggestionPanel;
	
	public SuggestingTextArea() {
		suggestionPanel = new SuggestionPanel(this);
		textArea.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				int pos = textArea.getCaretPosition();
				if (suggesting) {
					if (e.getKeyCode() == e.VK_RIGHT) {
						if (e.isAltDown())
							completeInLineSuggestion(selectedSuggestion);
						else
							addCharacter(suggestions.get(selectedSuggestion).charAt(suggestionIndex));
						e.consume();
					}
					else if (e.getKeyCode() == e.VK_LEFT) {
						failInLineSuggestion();
						e.consume();
					}
					else if (e.getKeyCode() == e.VK_UP) {
						selectedSuggestion = selectedSuggestion == 0 ? (suggestions.size() >= 10 ? 9 : suggestions.size() - 1) : selectedSuggestion - 1;
						suggestionPanel.updateSelection(selectedSuggestion);
						e.consume();
					}
					else if (e.getKeyCode() == e.VK_DOWN) {
						selectedSuggestion = selectedSuggestion == suggestionMax ? 0 : selectedSuggestion + 1;
						suggestionPanel.updateSelection(selectedSuggestion);
						e.consume();
					}
					else if (e.getKeyCode() == e.VK_ENTER) {
						completeInLineSuggestion(selectedSuggestion);
						e.consume();
					}
					else if (e.getKeyCode() == e.VK_BACK_SPACE) {
						failInLineSuggestion();
						pos--;
						if (pos > 0 && pos < textArea.getText().length())
							suggest0(pos, '\0');
					}
					else if (e.getKeyChar() >= 32 && e.getKeyChar() < 127)
						addCharacter(e.getKeyChar());
				}
				else if (e.getKeyChar() > 32 && e.getKeyCode() < 127)
					suggest0(textArea.getCaretPosition() + 1, e.getKeyChar());
			}
			
			@Override
			public void keyReleased(KeyEvent e) {}
		});
		textArea.setLocation(0, 0);
		add(textArea, JLayeredPane.DEFAULT_LAYER);
		add(suggestionPanel, JLayeredPane.PALETTE_LAYER);
		suggestionPanel.setVisible(false);
	}
	
	private final void suggest0(int pos, char c) {
		String text = c != '\0' ? textArea.getText() + c : textArea.getText();
		suggestionRoot = text.lastIndexOf(' ', pos) + 1;
		int temp = text.lastIndexOf('\n', pos) + 1;
		if (suggestionRoot < temp)
			suggestionRoot = temp;
		suggestions.clear();
		ArrayList<String> suggestions = suggest(text, suggestionRoot);
		int longest = 0;
		String test = text.substring(suggestionRoot, pos);
		for (int i = 0; i < suggestions.size(); i++) {
			if (!suggestions.get(i).startsWith(test)) {
				suggestions.remove(i--);
				continue;
			}
			if (suggestions.get(i).length() > longest)
				longest = suggestions.get(i).length();
			this.suggestions.add(i, suggestions.get(i));
		}
		if (this.suggestions.size() > 0) {
			suggestionPanel.updateSize(longest, suggestions.size() > 10 ? 10 : suggestions.size());
			beginSuggestion();
			suggestionIndex = pos - suggestionRoot;
		}
	}
	
	@Override
	public void setPreferredSize(Dimension dimension) {
		super.setPreferredSize(dimension);
		textArea.setSize(dimension);
	}
	
	public String getText() {
		return textArea.getText();
	}
	
	protected abstract ArrayList<String> suggest(String text, int caretPos);
	
	final void completeInLineSuggestion(int suggestion) {
		finishSuggestion();
		int pos = textArea.getCaretPosition();
		String text = textArea.getText();
		if (pos == text.length())
			textArea.setText(text.substring(0, pos) + suggestions.get(suggestion).substring(suggestionIndex));
		else
			textArea.setText(text.substring(0, pos) + suggestions.get(suggestion).substring(suggestionIndex) + text.substring(pos));
	}
	
	private final void failInLineSuggestion() {
		finishSuggestion();
	}
	
	private final void addCharacter(char c) {
		if (suggesting) {
			if (suggestions.get(0).charAt(suggestionIndex) == c) {
				if (suggestionIndex + 1 == suggestions.get(0).length()) {
					finishSuggestion();
					return;
				}
				for (int i = 1; i < suggestions.size(); i++)
					if (suggestions.get(i).charAt(suggestionIndex) != c)
						suggestions.remove(i--);
				suggestionIndex++;
				return;
			}
			for (int i = 0; i < suggestions.size(); i++)
				if (suggestions.get(i).charAt(suggestionIndex) != c)
					suggestions.remove(i--);
			suggestionIndex++;
			if (suggestions.size() == 0)
				failInLineSuggestion();
		}
	}
	
	private final void finishSuggestion() {
		suggesting = false;
		suggestionPanel.setVisible(false);
		for (KeyStroke keyStroke : keystrokes)
			textArea.getInputMap().put(keyStroke, null);
	}
	
	private final void beginSuggestion() {
		suggesting = true;
		suggestionIndex = 0;
		selectedSuggestion = 0;
		suggestionPanel.updateSelection(0);
		suggestionMax = suggestions.size() > 10 ? 9 : suggestions.size() - 1;
		try {
			Rectangle rect = textArea.modelToView(suggestionRoot);
			suggestionPanel.setLocation(rect.x, rect.y + fontHeight);
			suggestionPanel.setVisible(true);
		}
		catch (BadLocationException e) {
			e.printStackTrace();
		}
		for (KeyStroke keyStroke : keystrokes)
			textArea.getInputMap().put(keyStroke, "none");
		
	}
}

class SuggestionPanel extends JPanel {
	protected JList<String> suggestions;
	private SuggestingTextArea parent;
	
	public SuggestionPanel(SuggestingTextArea parent) {
		this.parent = parent;
		suggestions = new JList<String>(parent.suggestions);
		suggestions.setBackground(Color.gray);
		setFocusable(false);
		setOpaque(false);
		suggestions.setFocusable(false);
		add(suggestions);
		updateSize(new Dimension(parent.fontWidth * 10, parent.fontHeight * 10));
		suggestions.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				SuggestionPanel.this.parent.completeInLineSuggestion(suggestions.getSelectedIndex());
			}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
		});
	}
	
	void updateSelection(int index) {
		suggestions.setSelectedIndex(index);
	}
	
	void updateSize(Dimension dimension) {
		setSize(dimension);
		suggestions.setPreferredSize(dimension);
	}
	
	void updateSize(int longest, int rows) {
		Dimension dim = new Dimension(parent.fontWidth * longest, (parent.fontHeight + 4) * rows);
		suggestions.setPreferredSize(dim);
		setSize(dim);
	}
}
