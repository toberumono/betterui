package lipstone.joshua.betterUI.suggestion;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

public class TestSuggestions extends SuggestingTextArea {

	@Override
	protected ArrayList<String> suggest(String text, int caretPos) {
		ArrayList<String> suggestions = new ArrayList<String>();
		if (text.charAt(caretPos) == 't') {
			suggestions.add("test");
			suggestions.add("text");
			suggestions.add("tear");
		}
		return suggestions;
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("test");
		frame.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 5;
		c.gridwidth = 5;
		TestSuggestions textArea = new TestSuggestions();
		textArea.setPreferredSize(new Dimension(500, 500));
		frame.add(textArea, c);

		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridy = 5;
		frame.add(new JButton("Clear"), c);
		c.gridx = 4;
		frame.add(new JButton("Save"), c);
		
		frame.pack();
		frame.setVisible(true);
	}
}
