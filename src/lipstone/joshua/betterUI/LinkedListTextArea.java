package lipstone.joshua.betterUI;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import lipstone.joshua.lexer.Lexer;
import lipstone.joshua.lexer.Rule;
import lipstone.joshua.lexer.Token;
import lipstone.joshua.lexer.Type;

public class LinkedListTextArea extends JTextArea {
	protected Token list = new Token(), head = list;
	private BoundsList bounds = new BoundsList(), current = bounds;
	private boolean modified = false;
	protected final Lexer lexer = new Lexer();
	protected int wordStart = 0, location;
	private String splitChars = " \n\t\r";
	
	public LinkedListTextArea() {
		bounds.word = list;
		addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {
				location = getCaretPosition();
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (modified && location > -1) {
					int pos = getCaretPosition();
					try {
						head.append(new Token()); //TODO modify Token to speed this up
						lexer.lex(getText(), wordStart, list, head);
						System.out.println(list.toString());
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
					findBounds(location);
					modified = false;
					wordStart = previousBreak(pos) + 1;
				}
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
		});
		getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				//TODO write
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				if (e.getLength() > 1) {
					try {
						int offset = e.getOffset();
						shiftToBound(offset);
						lexer.lex(getText(), previousBreak(offset), list, head);
						findBounds(offset);
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
				}
				else {
					location--;
					if (splitChars.indexOf(getText().charAt(e.getOffset())) > -1) {
						(bounds = bounds.previous).next = bounds.next.next;
						bounds.next.previous = bounds; //double linkage
					}
				}
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				int pos = getCaretPosition();
				int keyChar = e.getKeyChar();
				if ((keyChar > 7 && keyChar < 14) || keyChar > 31)
					modified = true;
				if (splitChars.indexOf(e.getKeyChar()) > -1) {
					try {
						lexer.lex(getText().substring(wordStart), 0, list, head);
						System.out.println(list.toString());
					}
					catch (Exception e1) {
						e1.printStackTrace();
					}
					wordStart = pos + 1;
				}
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				int keyCode = e.getKeyCode();
				if (keyCode == e.VK_UP || keyCode == e.VK_DOWN || keyCode == e.VK_RIGHT || keyCode == e.VK_LEFT || keyCode == e.VK_BACK_SPACE || keyCode == e.VK_DELETE) {
					if (modified && location > -1) {
						int pos = getCaretPosition();
						try {
							head.append(new Token()); //TODO modify Token to speed this up
							lexer.lex(getText(), wordStart, list, head);
							System.out.println(list.toString());
						}
						catch (Exception e1) {
							e1.printStackTrace();
						}
						findBounds(location);
						modified = false;
						wordStart = previousBreak(pos) + 1;
					}
					location = getCaretPosition();
				}
				else
					location++;
			}
		});
	}
	
	/**
	 * Shifts from location to loc updating the current bound along the way
	 * 
	 * @param loc
	 *            the index to shift the bounds list to
	 */
	private final void shiftToBound(int loc) {
		int change = loc - location;
		if (change > 0)
			while (current.next != null && (change -= current.next.offset) > 0)
				current = current.next;
		if (change < 0)
			while (current.previous != null && (change += current.previous.offset) < 0)
				current = current.previous;
		head = current.word;
	}
	
	protected final int previousBreak(int index) {
		int location = index;
		String text = getText();
		for (char c : splitChars.toCharArray()) {
			int temp = text.lastIndexOf(c, index);
			if (temp > location)
				location = temp;
		}
		return location;
	}
	
	protected final int nextBreak(int index) {
		int location = index;
		String text = getText();
		for (char c : splitChars.toCharArray()) {
			int temp = text.indexOf(c, index);
			if (temp < location)
				location = temp;
		}
		return location;
	}
	
	/**
	 * This should be used after untrackable operations (select followed by a deletion, cut, or paste)
	 */
	private final void clearTokenization() {
		try {
			list = lexer.lex(getText());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		findBounds(0);
	}
	
	/**
	 * Overwrites the boundslist starting with the bound containing start
	 * 
	 * @param start
	 *            the index in the text at which to start scanning.
	 */
	private final void findBounds(int start) {
		BoundsList bound = bounds;
		int total = 0;
		if (start > 0) {
			while (true) {
				if ((total += bound.offset) > start) {
					bound = bound.previous;
					break;
				}
				if (bound.next == null)
					break;
				bound = bound.next;
			}
		}
		Token word = bound.word;
		while (true) {
			if ((total = nextBreak(total)) == -1)
				break;
			bound = bound.next = new BoundsList(bound, total - bound.offset);
			bound.word = word;
			word = word.getNextToken();
		}
		current = bound;
		shiftToBound(getCaretPosition());
	}
	
	public final Lexer getLexer() {
		return lexer;
	}
	
	public final Token getList() {
		return list;
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame("test");
		frame.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 5;
		c.gridwidth = 5;
		LinkedListTextArea textArea = new LinkedListTextArea();
		textArea.getLexer().addRule("all", new Rule(Pattern.compile(".+"), new Type("String")));
		textArea.setPreferredSize(new Dimension(500, 500));
		frame.add(textArea, c);
		
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridy = 5;
		frame.add(new JButton("Clear"), c);
		c.gridx = 4;
		frame.add(new JButton("Save"), c);
		
		frame.pack();
		frame.setVisible(true);
	}
}

class BoundsList {
	BoundsList next = null, previous;
	int offset;
	Token word;
	
	//This is for the initialization of the root item in this list
	public BoundsList() {
		offset = 0;
		previous = null;
	}
	
	public BoundsList(BoundsList previous, int delta) {
		this.previous = previous;
		previous.next = this;
		offset = previous.offset + delta;
	}
	
	public void update(int delta) {
		offset += delta;
	}
}
